<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "PagesController@index");

Route::get('/contacts',"PagesController@contacts");


// GET posts

Route::get('/posts', "PostsController@index");

Route::get('/posts/create', "PostsController@create");

Route::get('/posts/{post}/edit', "PostsController@edit");


//POST posts/{post}

Route::post('/posts/{post}', "PostsController@update");

// GET posts/{id}

Route::get('/posts/{post}', "PostsController@show");

// PUT posts

Route::put('/posts', "PostsController@store");

// DELETE posts/id

Route::delete('/posts/{post}', "PostsController@destroy");

Route::put('/posts/{post}/comments', "CommentsController@store");

//Users

Route::get('/users/create', "RegistrationController@create");

Route::put('/users', "RegistrationController@store");

Route::get('/logout', "SessionController@destroy");


Route::get('/login', "SessionController@create");

Route::post('/login', "SessionController@store");

//Shop:

Route::get('/products','ProductController@index');

Route::get('/products/{product}','ProductController@show');

Route::get('/cart/{product}', "CartController@store");

Route::get('/order/create', "OrderController@create");

Route::post('/order', "OrderController@store");

Route::get('/order/success', function (){
    return view('order.success');
});

Route::get('/order', "OrderController@index");


