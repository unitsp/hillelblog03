@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>{{$product->name}}</h2>
            <h2>{{$product->price}}</h2>
            <p>
                {{$product->short_description}}
            </p>
            <p>
                {{$product->description}}
            </p>
            <p><a class="btn btn-success" href="/cart/{{$product->alias}}" role="button">Купить »</a></p>

        </div>
    </div>
@endsection