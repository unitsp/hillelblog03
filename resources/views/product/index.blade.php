@extends('layouts.layout')

@section('content')
    <div class="row">
        @foreach($products AS $product)
            <div class="col-md-12">
                <h2>{{$product->name}}</h2>
                <p>{{$product->price}}</p>
                <p>{{$product->short_description}}</p>
                <p>{{$product->description}}</p>
                <p><a class="btn btn-default" href="/products/{{$product->alias}}" role="button">Подробнее »</a></p>
                <p><a class="btn btn-success" href="/cart/{{$product->alias}}" role="button">Купить »</a></p>

            </div>
        @endforeach
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Products :</h1>
    </div>
@endsection