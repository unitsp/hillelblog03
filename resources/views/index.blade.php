@extends('layouts.layout')

@section('content')
    <div class="row">
        @foreach($posts AS $post)
            <div class="col-md-12">
                <h2>{{$post->title}}</h2>
                <p>{{$post->intro}}</p>
                <p><a class="btn btn-default" href="/posts/{{$post->alias}}" role="button">Читать далее »</a></p>
                <p><a class="btn btn-warning" href="/posts/{{$post->alias}}/edit" role="button">Редактировать »</a></p>
                <p><form action="/posts/{{$post->alias}}" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Удалить »" class="btn btn-danger">
                </form></p>
            </div>
        @endforeach
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Hello, PHP!</h1>
    </div>
@endsection