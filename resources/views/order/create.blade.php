@extends('layouts.layout')

@section('headerBlock')
    <div class="container">
        <h1>Create new Order:</h1>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-8">

            <h2>Selected products:</h2>

            @foreach($products as $product)

                <h3>{{$product->name}} - {{$cart[$product->id]}}</h3>

            @endforeach

            <h2>Please enter delivery information:</h2>

            <form action="/order" method="post">

                {{csrf_field()}}

                <div class="form-group">
                    <label for="user_name">Your name:</label>
                    <input name="user_name" id="user_name" type="text" class="form-control">
                </div>

                <div class="form-group">
                    <label for="email">Your email:</label>
                    <input name="email" id="email" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="phone">Your phone:</label>
                    <input name="phone" id="phone" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="feedback">Feedback:</label>
                    <textarea name="feedback" id="feedback" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Submit order</button>
                </div>


            </form>

            @include('layouts.formError')

        </div>
    </div>


@endsection