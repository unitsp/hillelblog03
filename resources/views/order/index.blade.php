@extends('layouts.layout')

@section('headerBlock')
    <div class="container">
        <h1>Orders:</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        @foreach($orders AS $order)
            <div class="col-md-12">
                <h2>{{$order->user_name}}</h2>
                <p>{{$order->phone}}</p>
                <p>{{$order->email}}</p>
                <ul>
                    @foreach($order->products as $product)
                        <li>{{$product->name}} - {{$product->pricegit add}}</li>
                    @endforeach
                </ul>
            </div>
        @endforeach
    </div>
@endsection