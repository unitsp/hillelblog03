<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    public function store(Post $post)
    {
        Comment::create([
            'post_id' => $post->id,
            'body' => request('body')
        ]);
        return back();
    }

}
