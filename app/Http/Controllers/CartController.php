<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function store(Product $product){

        if(request()->cookie('cart')){
            $cart = json_decode(request()->cookie('cart'), true);

        }else{
            $cart = [];
        }

        if(isset($cart[$product->id])){
            $cart[$product->id]++;
        }else{
            $cart[$product->id] = 1;
        }
        return redirect('/products')->withCookie('cart', json_encode($cart));
    }
}
