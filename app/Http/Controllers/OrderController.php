<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('order.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cart = json_decode(request()->cookie('cart'), true);
        $products = [];
        foreach ($cart as $product_id => $amount) {
            $products[] = Product::find($product_id);
        }
        return view('order.create', compact('products', 'cart'));
    }

    public function store()
    {
        $this->validate(request(), [
            'user_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
        ]);

        $cart = json_decode(request()->cookie('cart'), true);

        $order = Order::create(request(['user_name', 'email', 'phone', 'feedback']));

        foreach ($cart as $product_id => $amount) {
            $order->products()->attach($product_id, ['amount' => $amount]);
        }

        $cookie = \Cookie::forget('cart');

        return redirect('/order/success')->withCookie($cookie);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
