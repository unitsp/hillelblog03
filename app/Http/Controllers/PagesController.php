<?php

namespace App\Http\Controllers;

use App\Post as Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    public function index()
    {

        $data['posts'] = Post::all();
        return view('index', $data);

    }

//    public function posts($id){
//        $post = Post::find($id);

    public function posts(Post $post){
        $data['post'] = $post;
        return view('posts.show')->with($data);

    }

    public function contacts()
    {
        return view('contacts');
    }

    public function delete($id){

        Post::destroy($id);
        return redirect('/');

    }


}
