<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostsController extends Controller
{
    // Показывать список записей
    public function index()
    {
        $data['posts'] = Post::all();
        return view('index', $data);
    }

    // Показывать конкретную запись
    public function show(Post $post){

        return view('posts.show')->with(compact('post'));

    }

    // 3.Создавать запись

    // 3.1 Показать форму

    public function create(){

        return view('posts.create');

    }

    // 3.2 Получить и обработать данные из формы

    public function store(){

        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'body' => 'required',
        ]);

        Post::create(request(['title', 'alias', 'intro', 'body']));

        return redirect('/posts');


    }



    // 4 Редактировать запись

    // 4.1 Показать форму редактирования

    public function edit(Post $post){
        return view('posts.edit', compact('post'));
    }

    // 4.2 Обработать форму редактирования

    public function update(Post $post){
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'body' => 'required',
        ]);

        $post->update(request()->all());

        return redirect('/');

    }

    // Удалять запись
    public function destroy(Post $post){

        $post->delete();
        return redirect('/');

    }
}
