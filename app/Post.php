<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * If we need to work with custom table
     */

    protected $fillable = ['title', 'alias', 'intro', 'body'];

    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function comments(){

        return $this->hasMany(Comment::class);

    }

    public function user(){

        return $this->belongsTo(User::class);

    }

}
