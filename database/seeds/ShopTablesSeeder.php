<?php

use Illuminate\Database\Seeder;

class ShopTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'name' => 'Macbook Air',
                'alias' => 'macbook_air',
                'price' => 1000,
                'short_description' => 'Slim and cool laptop',
                'description' => 'Slim and cool laptop, Slim and cool laptop, Slim and cool laptop'
            ],
            [
                'name' => 'Macbook Pro',
                'alias' => 'macbook_pro',
                'price' => 2000,
                'short_description' => 'cool laptop',
                'description' => 'cool laptop'
            ],
            [
                'name' => 'Iphone',
                'alias' => 'iphone',
                'price' => 600,
                'short_description' => 'cool smart phone',
                'description' => 'cool smart phone cool smart phone cool smart phone'
            ],
        ]);
    }
}
