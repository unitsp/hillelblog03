<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
                array(
                    'title' => 'Php is awesome',
                    'user_id' => 1,
                    'alias' => 'php_is_awesone',
                    'intro' => 'The PHP development team announces the immediate availability of PHP 7.1.5. Several bugs have been fixed. All PHP 7.1 users are encouraged to upgrade to this version.',
                    'body' => 'The PHP development team announces the immediate availability of PHP 7.1.5. Several bugs have been fixed. All PHP 7.1 users are encouraged to upgrade to this version.For source downloads of PHP 7.1.5 please visit our downloads page, Windows source and binaries can be found on windows.php.net/download/. The list of changes is recorded in the ChangeLog.'
                ),
                array(
                    'title' => 'Laravel 5.4 released!',
                    'user_id' => 1,
                    'alias' => 'laravel',
                    'intro' => 'Love beautiful code? We do too.',
                    'body' => 'Value elegance, simplicity, and readability? You’ll fit right in. Laravel is designed for people just like you. If you need help getting started, check out Laracasts and our great documentation.'
                ),
                array(
                    'title' => 'Seeders helps us to fill database',
                    'user_id' => 1,
                    'alias' => 'seeders',
                    'intro' => 'Love beautiful code',
                    'body' => 'Value elegance, simplicity, and readability? You’ll fit right in. Laravel is designed for people just like you. If you need help getting started, check out Laracasts and our great documentation.'
                )]
        );
    }
}
